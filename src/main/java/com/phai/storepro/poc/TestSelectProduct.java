/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.storepro.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Product;

/**
 *
 * @author Phai
 */
public class TestSelectProduct {

   public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT"); 
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product pro = new Product(id,name,price);
                System.out.println(pro);
            }
            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        
    }
}
