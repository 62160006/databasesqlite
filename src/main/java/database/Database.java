/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phai
 */
public class Database {

    private static Database instance = new Database();
    private Connection con;

    private Database() {

    }

    public static Database getInstance() {

        String dbName = "./db/store.db";
        try {
            if (instance.con == null || instance.con.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.con = DriverManager.getConnection("jdbc:sqlite:" + dbName);
                System.out.println("Connect database " + dbName + " sucessfully");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Library not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Connect database " + dbName + " failed");
            System.exit(0);
        }
        
        return instance;
    }

    public static void close() {
        try {
            if (instance.con != null && !instance.con.isClosed()) {
                instance.con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.con = null;
    }
    public Connection getConnecttion(){
        return instance.con;
    }
}
