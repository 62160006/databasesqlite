/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author Phai
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int id = -1;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO USER (NAME,TEL,PASSWORD) "
                    + "VALUES ('" + object.getName() + "'," + object.getTel() + ",'" + object.getPassword() + "');";
            stmt.executeUpdate(sql);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            conn.commit();
            stmt.close();
            conn.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
        return -1;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection c = null;
        Statement stmt = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String password = rs.getString("password");
                User pro = new User(id, name, tel, password);
                System.out.println(pro);
                list.add(pro);
            }
            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection c = null;
        Statement stmt = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER WHERE id=" + id);
            if (rs.next()) {
                int idp = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String password = rs.getString("password");
                User pro = new User(id, name, tel, password);
                System.out.println(pro);
                return pro;
            }
            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public void delete(int id) {
        Connection c = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM USER WHERE ID =" + id);
            c.commit();
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");

            while (rs.next()) {
                int idp = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String password = rs.getString("password");
                User pro = new User(id, name, tel, password);
                System.out.println(pro);
            }
            rs.close();
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        System.out.println("Records created successfully");
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE USER set PASSWORD =" + object.getPassword() + " where ID=" + object.getId() + "");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1,"Tong", "0874896258","axdza"));
        System.out.println("id "+id);
        User lastPro = dao.get(1);
        System.out.println("last "+lastPro);
        lastPro.setPassword("12345678");
        int row = dao.update(lastPro);
        User updatePro = dao.get(id);
        System.out.println("update "+updatePro);
        dao.delete(id);
        User deletePro = dao.get(id);
        System.out.println("daelte "+ deletePro);
    }

}
