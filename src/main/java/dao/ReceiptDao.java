/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author Phai
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Statement stmt = null;
        Statement stmtdetail = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int id = -1;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO RECEIPT (CUSTOMER_ID,USER_ID,TOTAL) "
                    + "VALUES (" + object.getCustomer().getId() + "," + object.getSeller().getId() + "," + object.getTotal() + ");";
            stmt.executeUpdate(sql);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptdetail()) {
                stmtdetail = conn.createStatement();
                String sqla = "INSERT INTO RECEIPT_DETAIl (RECEIPT_ID,PRODUCT_ID,PRICE,AMOUNT) "
                        + "VALUES (" + r.getReceipt().getId() + "," + r.getProduct().getId() + "," + r.getPeice() + "," + r.getAmount() + ");";
                stmtdetail.executeUpdate(sqla);
                ResultSet resultdetail = stmtdetail.getGeneratedKeys();
                if (resultdetail.next()) {
                    id = resultdetail.getInt(1);
                    r.setId(id);
                }
            }
            conn.commit();
            stmt.close();
            conn.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
        return -1;

    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        Statement stmt = null;
        conn = db.getConnecttion();
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Receipt");
            
            while (rs.next()) {
                int id = rs.getInt("id");
                Date created = rs.getDate("created");
                int cusId = rs.getInt("customer_id");
                int userId = rs.getInt("user_id");
                double total = rs.getDouble("total");
                CustomerDao cusDao = new CustomerDao();
                Customer customer = cusDao.get(cusId);
                UserDao userDao = new UserDao();
                User seller = userDao.get(userId);
                Receipt pro = new Receipt( id, created, seller, customer);
                list.add(pro);
            }

            rs.close();
            stmt.close();
            conn.commit();
            conn.close();
        
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt");
        } catch (Exception ex){
            System.out.println("ErrorErrorErrorError");
        }
        return list;
    }

    @Override
    public Receipt get(int id) {

        Connection c = null;
        Statement stmt = null;
        Statement stmtdetail = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM RECEIPT WHERE id=" + id);

            if (rs.next()) {
                int ids = rs.getInt("id");
                Date created = rs.getDate("created");
                int cusId = rs.getInt("customer_id");
                int userId = rs.getInt("user_id");
                double total = rs.getDouble("total");
                CustomerDao cusDao = new CustomerDao();
                Customer customer = cusDao.get(cusId);
                UserDao userDao = new UserDao();
                User seller = userDao.get(userId);
                Receipt pro = new Receipt( id, created, seller, customer);
                pro.setTotal(total);
                
                ResultSet rd = stmt.executeQuery("SELECT * FROM RECEIPT_DETAIL WHERE receipt_id"+ids);
                while(rd.next()){
                    int RDetailId = rd.getInt("id");
                    int pid = rd.getInt("product_id");
                    double price = rd.getDouble("price");
                    int amount = rd.getInt("amount");
                    ProductDao proDao = new ProductDao();
                    Product product = new Product(pid, proDao.get(pid).getName(),proDao.get(pid).getPrice());
                    pro.addReceiptDetail(product, amount);
                }
                
                return pro;
            }

            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return null;
    }

    

    
    

    @Override
    public int update(Receipt object) {
//        Connection conn = null;
//        Statement stmt = null;
//        Database db = Database.getInstance();
//        conn = db.getConnecttion();
//        int row = 0;
//        try {
//            db.getConnecttion();
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//            row = stmt.executeUpdate("UPDATE PRODUCT set PRICE ="+object.getPrice()+" where ID="+object.getId()+"");
//            conn.commit();
//            stmt.close();
//            conn.close();
//        } catch (SQLException ex) {
//            System.out.println("Unable to open database");
//            System.exit(0);
//        }
//        //System.out.println("Opened database successfully");
//        db.close();
        return 0;
    }
    @Override
    public void delete(int id) {
        Connection c = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM RECEIPT WHERE ID ="+id);
            c.commit();
            ResultSet rs = stmt.executeQuery("SELECT * FROM RECIPT");

            while (rs.next()) {
                int pid = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product pro = new Product(id,name,price);
                System.out.println(pro);
            }
            rs.close();
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        System.out.println("Records created successfully");
    }
    public static void main(String[] args) {
        Product p1 = new Product(1, "Coco", 75);
        Product p2 = new Product(2, "Coffee", 55);
        User seller = new User(1, "Phai", "0874896258", "12345678");
        Customer customer = new Customer("CREAM", "8886489460");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 1);

        ReceiptDao dao = new ReceiptDao();
        dao.add(receipt);
        dao.getAll();
        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("new " + newReceipt);
        dao.delete(0);
    }

    

}
