/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author Phai
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int id = -1;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO PRODUCT (NAME,PRICE) "
                    + "VALUES ('" + object.getName() + "'," + object.getPrice() + ");";
            stmt.executeUpdate(sql);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            conn.commit();
            stmt.close();
            conn.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
        return -1;

    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection c = null;
        Statement stmt = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product pro = new Product(id, name, price);
                System.out.println(pro);
                list.add(pro);
            }
            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return list;

    }

    @Override
    public Product get(int id) {

        Connection c = null;
        Statement stmt = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT WHERE id=" + id);
            if (rs.next()) {
                int pid = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product pro = new Product(id, name, price);
                System.out.println(pro);
                return pro;
            }
            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public void delete(int id) {
        Connection c = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM PRODUCT WHERE ID ="+id);
            c.commit();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT");

            while (rs.next()) {
                int pid = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product pro = new Product(id,name,price);
                System.out.println(pro);
            }
            rs.close();
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        System.out.println("Records created successfully");
    }
    @Override
    public int update(Product object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE PRODUCT set PRICE ="+object.getPrice()+" where ID="+object.getId()+"");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }

    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1,"Banana", 200.0));
        System.out.println("id "+id);
        Product lastPro = dao.get(1);
        System.out.println("last "+lastPro);
        lastPro.setPrice(65);
        int row = dao.update(lastPro);
        Product updatePro = dao.get(id);
        System.out.println("update "+updatePro);
        dao.delete(id);
        Product deletePro = dao.get(id);
        System.out.println("daelte "+ deletePro);
    }

}
