/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Customer;

/**
 *
 * @author Phai
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int id = -1;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO CUSTOMER (NAME,TEL) "
                    + "VALUES ('" + object.getName() + "'," + object.getTel() + ");";
            stmt.executeUpdate(sql);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            conn.commit();
            stmt.close();
            conn.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
        return -1;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection c = null;
        Statement stmt = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM CUSTOMER");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                Customer pro = new Customer(id, name, tel);
                System.out.println(pro);
                list.add(pro);
            }
            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection c = null;
        Statement stmt = null;
        String dbName = "./db/store.db";
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM CUSTOMER WHERE id=" + id);
            if (rs.next()) {
                int pid = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                Customer pro = new Customer(id, name, tel);
                System.out.println(pro);
                return pro;
            }
            rs.close();
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public void delete(int id) {
        Connection c = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        c = db.getConnecttion();
        try {
            db.getConnecttion();
            c.setAutoCommit(false);
            stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM CUSTOMER WHERE ID =" + id);
            c.commit();
            ResultSet rs = stmt.executeQuery("SELECT * FROM CUSTOMER");

            while (rs.next()) {
                int pid = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                Customer pro = new Customer(id, name, tel);
                System.out.println(pro);
            }
            rs.close();
            stmt.close();

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        System.out.println("Records created successfully");
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE CUSTOMER set TEL =" + object.getTel() + " where ID=" + object.getId() + "");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }
    
     public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1,"PHAI", "0861409478"));
        System.out.println("id "+id);
        Customer lastPro = dao.get(1);
        System.out.println("last "+lastPro);
        lastPro.setTel("08886489460");
        int row = dao.update(lastPro);
        Customer updatePro = dao.get(id);
        System.out.println("update "+updatePro);
        dao.delete(id);
        Customer deletePro = dao.get(id);
        System.out.println("daelte "+ deletePro);
    }

}
